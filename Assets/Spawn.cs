﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public GameObject obstacle;
    public float speed = 10000;

    void SpawnObstacle() {
        GameObject obstacleClone = Instantiate(obstacle, this.transform.position, this.transform.rotation);
        obstacleClone.GetComponent<Rigidbody>().AddForce(Vector3.forward * -1 * speed);
        Destroy(obstacleClone, 2);
    }

    void Strafe() {
        transform.position = new Vector3(Mathf.PingPong(Time.time * 10, 6) - 3, transform.position.y, transform.position.z);
    }

    void Update() {
        SpawnObstacle();
        Strafe();
    }
}
