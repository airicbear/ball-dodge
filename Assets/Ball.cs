﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public float speed = 500;
    
    void Strafe() {
        this.GetComponent<Rigidbody>().velocity = Vector3.right * Input.GetAxisRaw("Horizontal") * this.speed * Time.deltaTime;
    }
    
    void FixedUpdate()
    {
        if (this.GetComponent<Rigidbody>().velocity.y == 0) {
            Strafe();
        }
    }
}
